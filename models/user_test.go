package models

import (
	"testing"
)

func TestGetTurningAction(t *testing.T) {
	var action string

	action = GetTurningAction(TOP, RIGHT)
	if action != TURN_RIGHT {
		t.Errorf("from %s to %s get: %s", TOP, RIGHT, action)
	}

	action = GetTurningAction(TOP, LEFT)
	if action != TURN_LEFT {
		t.Errorf("from %s to %s get: %s", TOP, LEFT, action)
	}

	action = GetTurningAction(RIGHT, BOTTOM)
	if action != TURN_RIGHT {
		t.Errorf("from %s to %s get: %s", RIGHT, BOTTOM, action)
	}

	action = GetTurningAction(LEFT, RIGHT)
	if action != TURN_LEFT {
		t.Errorf("from %s to %s get: %s", LEFT, RIGHT, action)
	}

	action = GetTurningAction(BOTTOM, LEFT)
	if action != TURN_RIGHT {
		t.Errorf("from %s to %s get: %s", BOTTOM, LEFT, action)
	}
}

func TestGetSafeCoordsForPlayer(t *testing.T) {
	api := &ApiMock{}

	b := Bullet{
		Unit: Unit{
			Coords: Coords{
				X: 6,
				Y: 6,
			},
			Direction: TOP,
		},
		ID: "234",
	}

	b2 := Bullet{
		Unit: Unit{
			Coords: Coords{
				X: 6,
				Y: 7,
			},
			Direction: BOTTOM,
		},
		ID: "wer",
	}

	p := Player{
		Unit: Unit{
			Coords: Coords{
				X: 6,
				Y: 5,
			},
			Direction: BOTTOM,
		},
		Status: ACTIVE,
	}

	store := NewStore(api)
	store.State = &State{
		Bullets: []Bullet{
			b,
			b2,
		},
	}

	safeLines := store.GetSafeCoordsForPlayer(&p.Unit)

	if b.Direction == TOP || b.Direction == BOTTOM {
		for _, x := range safeLines.XList {
			if x == b.X {
				t.Errorf("Got bullet.X %d in %v", b.X, safeLines.XList)
			}
		}
	}

	if b.Direction == LEFT || b.Direction == RIGHT {
		for _, y := range safeLines.YList {
			if y == b.X {
				t.Errorf("Got bullet.Y %d in %v", b.Y, safeLines.YList)
			}
		}
	}
}

func TestNearest(t *testing.T) {
	x := 5
	items := []int{1, 3, 8, 6, 13}

	n := Nearest(x, items)

	if n != 6 {
		t.Errorf("Got %d for %d in %v", n, x, items)
	}
}

type ApiMock struct{}

func (ApiMock) DoAction(action string) error {
	return nil
}

func (ApiMock) GetStatus() (*State, error) {

	return &State{}, nil
}
