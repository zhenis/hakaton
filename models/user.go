package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"reflect"
	"sync"
	"time"
)

const (
	API_URL    = "https://fight.codetau.com/api"
	TOP        = "top"
	BOTTOM     = "bottom"
	LEFT       = "left"
	RIGHT      = "right"
	ACTIVE     = "active"
	DESTROYED  = "destroyed"
	FORWARD    = "forward"
	TURN_RIGHT = "rotate-right"
	TURN_LEFT  = "rotate-left"
	SHOT       = "shot"
)

type ApiClient interface {
	GetStatus() (*State, error)
	DoAction(action string) error
}

type Api struct {
	token  string
	client *http.Client
}

func NewApi(client *http.Client, token string) *Api {
	return &Api{
		token:  token,
		client: client,
	}
}

func (a *Api) GetStatus() (*State, error) {

	state := &State{}

	res, err := a.client.Get(API_URL + "/status")
	if err != nil {
		return nil, err
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return nil, readErr
	}

	fmt.Println("STATUS RESPONSE: ", string(body))
	fmt.Println()

	jsonErr := json.Unmarshal(body, state)

	if jsonErr != nil {
		return nil, jsonErr
	}

	return state, nil
}

func (api *Api) DoAction(action string) error {
	req := map[string]interface{}{
		"api_key": api.token,
		"action":  action,
	}

	fmt.Println("ACTION REQUEST: ", req)
	fmt.Println()

	res1B, _ := json.Marshal(req)
	data := []byte(res1B)
	r := bytes.NewReader(data)

	resp, err := api.client.Post(API_URL+"/action", "application/json", r)

	if err != nil {
		return err
	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	if action != SHOT {
		time.Sleep(time.Millisecond * 300)
	}

	return nil
}

type AuthToken struct {
	Succes        bool   `json:"success"`
	Player1ApiKey string `json:"player1_api_key"`
	Player2ApiKey string `json:"player2_api_key"`
	Message       string `json:"message"`
}

type Coords struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type Unit struct {
	Coords
	Direction string `json:"direction"`
}

type Player struct {
	Unit
	Status string `json:"status"`
}

type Bullet struct {
	Unit
	ID string
}

type State struct {
	PlayerOne  *Player  `json:"player1"`
	PlayerTwo  *Player  `json:"player2"`
	Bullets    []Bullet `json:"bullets"`
	Explosions []Coords `json:"explosions"`
}

type Store struct {
	sm    *sync.Mutex
	State *State
	api   ApiClient
}

func (s *Store) UpdateStatus() (bool, error) {

	state, err := s.api.GetStatus()
	if err != nil {
		return false, err
	}

	isEqual := reflect.DeepEqual(*s.State, *state)

	s.sm.Lock()
	s.State = state
	s.sm.Unlock()

	return !isEqual, nil
}

func (s *Store) IsPlayerOppositeToBullets(unit *Unit) bool {
	for _, bullet := range s.State.Bullets {

		if IsUnitIppositeToOther(unit, &bullet.Unit) {
			return true
		}
		// if bullet.X == unit.X || bullet.Y == unit.Y {
		// 	return true
		// }
	}
	return false
}

type EmptyLines struct {
	XList []int
	YList []int
}

func (s *Store) GetSafeCoordsForPlayer(unit *Unit) *EmptyLines {
	XList := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	YList := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}

	for _, bullet := range s.State.Bullets {
		// if bullet.Direction != unit.Direction {
		if bullet.X < unit.X && bullet.Direction == LEFT {
			continue
		} else if bullet.X > unit.X && bullet.Direction == RIGHT {
			continue
		} else if bullet.Y < unit.Y && bullet.Direction == TOP {
			continue
		} else if bullet.Y > unit.Y && bullet.Direction == BOTTOM {
			continue
		}

		if bullet.Direction == TOP || bullet.Direction == BOTTOM {
			for i := 0; i < len(XList); i++ {
				if XList[i] == bullet.X {
					XList = append(XList[:i], XList[i+1:]...)
				}
			}
		} else if bullet.Direction == LEFT || bullet.Direction == RIGHT {

			for i := 0; i < len(YList); i++ {
				if YList[i] == bullet.Y {
					YList = append(YList[:i], YList[i+1:]...)
				}
			}
		}
	}

	return &EmptyLines{
		XList,
		YList,
	}
}

func NewStore(api ApiClient) *Store {
	return &Store{
		sm:  &sync.Mutex{},
		api: api,
		State: &State{
			&Player{
				Unit{
					Coords{
						0,
						0,
					},
					TOP,
				},
				ACTIVE,
			},
			&Player{
				Unit{
					Coords{
						0,
						0,
					},
					TOP,
				},
				ACTIVE,
			},
			make([]Bullet, 15),
			make([]Coords, 2),
		},
	}
}

func Nearest(n int, elems []int) int {
	res := 0
	diff := 20

	for _, el := range elems {
		diffNow := int(math.Abs(float64(el - n)))

		if diffNow < diff {
			res = el
			diff = diffNow
		}
	}

	return res
}

func GoShot(playerIndex int, store *Store, api *Api) {
	fmt.Println("GoShot")

	for {
		var player *Player
		var enemy *Player

		if playerIndex == 1 {
			player = store.State.PlayerOne
			enemy = store.State.PlayerTwo
		} else {
			player = store.State.PlayerTwo
			enemy = store.State.PlayerOne
		}

		fmt.Println("GoShot: LOOP")

		if IsUnitIppositeToOther(&player.Unit, &enemy.Unit) {
			api.DoAction(SHOT)
		}
		time.Sleep(time.Millisecond * 500)
	}
}

func GoFor(stateChanged chan bool, playerIndex int, store *Store, api *Api) {

	for range stateChanged {

		fmt.Println("GoFor: =============")
		u := &Unit{}
		var player *Player
		var enemy *Player

		if playerIndex == 1 {
			player = store.State.PlayerOne
			enemy = store.State.PlayerTwo
		} else {
			player = store.State.PlayerTwo
			enemy = store.State.PlayerOne
		}

		if enemy.Status == DESTROYED {
			fmt.Println("+++++++ I WON!!!!++++++")
			break
		}

		if player.Status == DESTROYED {
			fmt.Println("+++++++ I LOST ....++++++")
			break
		}

		xDiff := math.Abs(float64(player.X - enemy.X))
		yDiff := math.Abs(float64(player.Y - enemy.Y))

		if xDiff > yDiff {
			u.Y = enemy.Y
			u.X = player.X
			if enemy.X > player.X {
				u.Direction = RIGHT
			} else {
				u.Direction = LEFT
			}

		} else {
			u.Y = player.Y
			u.X = enemy.X

			if enemy.Y > player.Y {
				u.Direction = BOTTOM
			} else {
				u.Direction = TOP
			}
		}

		if store.IsPlayerOppositeToBullets(u) {
			emptyLines := store.GetSafeCoordsForPlayer(u)

			fmt.Println("emptyLines: ", *emptyLines)
			fmt.Println("emptyLines: BEFORE U ", *u)

			u.X = Nearest(u.X, emptyLines.XList)
			u.Y = Nearest(u.Y, emptyLines.YList)

			fmt.Println("emptyLines: AFTER U ", *u)
		}

		fmt.Println("PLAYER: ")
		fmt.Printf("Pri: %#v", player)
		fmt.Println()

		fmt.Println("POSITION: ")
		fmt.Println(u)
		fmt.Println()

		if player.Y == u.Y {
			if player.X < u.X {
				if player.Direction != RIGHT {
					action := GetTurningAction(player.Direction, RIGHT)

					if action != "" {

						api.DoAction(action)
					}
					continue
				}

				api.DoAction(FORWARD)
				continue
			} else if player.X > u.X {
				if player.Direction != LEFT {
					action := GetTurningAction(player.Direction, LEFT)
					if action != "" {

						api.DoAction(action)
					}
					continue
				}

				api.DoAction(FORWARD)
				continue
			}
		} else if player.X == u.X {

			if player.Y < u.Y {
				if player.Direction != BOTTOM {
					action := GetTurningAction(player.Direction, BOTTOM)
					if action != "" {

						api.DoAction(action)
					}
					continue
				}

				api.DoAction(FORWARD)
				continue
			} else if player.Y > u.Y {
				if player.Direction != TOP {
					action := GetTurningAction(player.Direction, TOP)
					if action != "" {

						api.DoAction(action)
					}
					continue
				}

				api.DoAction(FORWARD)
				continue
			}
		}

		if player.X == u.X && player.Y == u.Y && player.Direction != u.Direction {
			action := GetTurningAction(player.Direction, u.Direction)
			if action != "" {

				api.DoAction(action)
			}
			continue
		}

		if player.X == u.X && player.Y == u.Y && player.Direction == u.Direction {

			if IsUnitIppositeToOther(&player.Unit, &enemy.Unit) {
				api.DoAction(SHOT)
			}
			continue
		}

	}
}

func IsUnitIppositeToOther(u1 *Unit, u2 *Unit) bool {
	if u1.X == u2.X {
		if u1.Y < u2.Y && u1.Direction == BOTTOM {
			return true
		} else if u1.Y > u2.Y && u1.Direction == TOP {
			return true
		}
	} else if u1.Y == u2.Y {
		if u1.X < u2.X && u1.Direction == RIGHT {
			return true
		} else if u1.X > u2.X && u1.Direction == LEFT {
			return true
		}
	}

	return false
}

func GetTurningAction(fromDirection, toDirection string) string {

	switch fromDirection {
	case TOP:
		switch toDirection {
		case LEFT:
			return TURN_LEFT
		case RIGHT:
			return TURN_RIGHT
		case BOTTOM:
			return TURN_LEFT
		}
	case BOTTOM:
		switch toDirection {
		case LEFT:
			return TURN_RIGHT
		case RIGHT:
			return TURN_LEFT
		case TOP:
			return TURN_LEFT
		}
	case LEFT:
		switch toDirection {
		case RIGHT:
			return TURN_LEFT
		case TOP:
			return TURN_RIGHT
		case BOTTOM:
			return TURN_LEFT
		}
	case RIGHT:
		switch toDirection {
		case TOP:
			return TURN_LEFT
		case BOTTOM:
			return TURN_RIGHT
		case LEFT:
			return TURN_LEFT
		}
	}

	return ""
}
