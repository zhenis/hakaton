package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/zhenis/hakaton/models"
)

func main() {
	args := os.Args[1:]

	token := args[1]

	if token == "" {
		log.Fatal(errors.New("token not provided"))
	}

	playerIndex, err := strconv.Atoi(args[0])

	if err != nil {
		log.Fatal(errors.New("id is not a number"))
	}

	if playerIndex != 1 && playerIndex != 2 {
		log.Fatal(errors.New("id is not equal to 1 or 2"))
	}

	client := &http.Client{}
	api := models.NewApi(client, token)

	store := models.NewStore(api)

	stateChanged := make(chan bool, 1)
	go func() {
		for {

			changed, err := store.UpdateStatus()

			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println("stateChanged : ", changed)

			if changed {
				stateChanged <- changed
			}

			if store.State.PlayerOne.Status == models.DESTROYED || store.State.PlayerTwo.Status == models.DESTROYED {
				close(stateChanged)
				os.Exit(0)
			}

			time.Sleep(time.Millisecond * 200)
		}

	}()

	// go models.GoShot(playerIndex, store, api)

	stateChanged <- true
	models.GoFor(stateChanged, playerIndex, store, api)

}
